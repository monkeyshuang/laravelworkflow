<?php
namespace App\Work\Repositories;

use App\Work\Model\User;
use Illuminate\Support\Facades\DB;

class UserRepo{
    /**
	 * 获取用户信息
	 *
	 * @param $wf_type
	 */
	public static function getUser() 
	{
        $user_table  =  [
            'user'=>['user','id','username','id as id,username as username','username'],
            'role'=>['role','id','name','id as id,name as username','name']
        ];
		return  User::select('id as id','username as username')->get();
	}
	public static function getRole() 
	{
		return  Role::select('id as id ','name as username')->get();
	}
	public static function ajaxGet($type,$keyword){
		if($type=='user'){
			$map['username']  = array('like','%'.$keyword.'%');
			return User::select('id as id','username as username')->where($map)->get();
        }else{
			$map['name']  = array('like','%'.$keyword.'%');
			return Role::select('id as id','name as username')->where($map)->get();
        }
	}
	
	public static function GetUserInfo($id) 
	{
		require ( BEASE_URL . '/config/config.php');// 
		return  DB::table($user_table['user'][0])->where($user_table['user'][1],$id)->select($user_table['user'][3])->get();
	}
}